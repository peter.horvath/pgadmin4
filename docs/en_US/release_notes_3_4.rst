***********
Version 3.4
***********

Release date: 2018-10-04

This release contains a number of features and fixes reported since the release of pgAdmin4 3.3


Features
********

| `Feature #2927 <https://redmine.postgresql.org/issues/2927>`_ - Move all CSS into SCSS files for consistency and ease of colour maintenance etc.

Bug fixes
*********

